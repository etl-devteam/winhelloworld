; HelloWorld.nsi
;
; It will install "HelloWorld" application for Windows into a directory that the user selects,
; creating start menu and desktop links, according to user choice.

;--------------------------------


; The name of the installer
Name "VisionAR Hello World"

; The file to write
OutFile "visionarhelloworld.exe"

; Installer Icon
Icon "VisionAR.ico"

; Request application privileges for Windows Vista and higher
RequestExecutionLevel admin

; Build Unicode installer
Unicode True

; The default installation directory
InstallDir "C:\ETL\VisionARHelloWorld"

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\VisionARHelloWorld" "Install_Dir"

;--------------------------------

!include CheckAndDownloadVCRedist.nsh
!include CheckAndDownloaddotnet.nsh

;--------------------------------

; Pages

Page components
Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------

; The stuff to install
Section "VisionAR HelloWorld (required)"

  SectionIn RO
  
  Call CheckAndDownloadVCRedist2015
  Call CheckAndDownloadDotNet472

  CreateDirectory "$INSTDIR"
  
  ; Set output path to the installation directory.
  SetOutPath "$INSTDIR"
  
  ; Put file there
  File "HelloWorld\bin\Release\HelloWorld.exe.config"
  File "HelloWorld\bin\Release\VisionARCDC.dll"
  File "HelloWorld\bin\Release\VisionARFB.dll"
  File "HelloWorld\bin\Release\HelloWorld.exe"
  File "VisionAR.ico"

  ; Write the installation path into the registry
  WriteRegStr HKLM SOFTWARE\VisionARHelloWorld "Install_Dir" "$INSTDIR"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARHelloWorld" "DisplayName" "VisionAR Hello, World"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARHelloWorld" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARHelloWorld" "InstallLocation" '"$INSTDIR"'
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARHelloWorld" "Publisher" "Univet s.r.l."
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARHelloWorld" "URLInfoAbout" "www.univetar.com"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARHelloWorld" "DisplayVersion" "2.2"
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARHelloWorld" "VersionMajor" 2
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARHelloWorld" "VersionMinor" 2
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARHelloWorld" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARHelloWorld" "NoRepair" 1
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARHelloWorld" "DisplayIcon" "$INSTDIR\VisionAR.ico,0"


  WriteUninstaller "$INSTDIR\uninstall.exe"
  
SectionEnd


; Optional section (can be disabled by the user)
Section "Start Menu Shortcuts"

  ; Set shortcut working path to the installation directory.
  SetOutPath "$INSTDIR"

  CreateDirectory "$SMPROGRAMS\VisionARHelloWorld"
  CreateShortcut "$SMPROGRAMS\VisionARHelloWorld\Uninstall.lnk" "$INSTDIR\uninstall.exe"
  CreateShortcut "$SMPROGRAMS\VisionARHelloWorld\VisionAR Hello World.lnk" "$INSTDIR\HelloWorld.exe" "" "$INSTDIR\VisionAR.ico"

SectionEnd

; Optional section (can be disabled by the user)
Section "Desktop Shortcut"

  ; Set shortcut working path to the installation directory.
  SetOutPath "$INSTDIR"
  
  CreateShortcut "$DESKTOP\VisionAR Hello World.lnk" "$INSTDIR\HelloWorld.exe" "" "$INSTDIR\VisionAR.ico"

SectionEnd

;--------------------------------

; Uninstaller

Section "Uninstall"
  
  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARHelloWorld"
  DeleteRegKey HKLM SOFTWARE\VisionARHelloWorld

  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\VisionAR Hello World\*.lnk"
  Delete "$DESKTOP\VisionAR Hello World.lnk"

  ; Remove files and uninstaller
  Delete "$INSTDIR\HelloWorld.exe.config"
  Delete "$INSTDIR\VisionARCDC.dll"
  Delete "$INSTDIR\VisionARFB.dll"
  Delete "$INSTDIR\HelloWorld.exe"
  Delete "$INSTDIR\uninstall.exe"

  ; Remove directories
  RMDir "$SMPROGRAMS\VisionARHelloWorld"
  RMDir /r "$INSTDIR"

SectionEnd
