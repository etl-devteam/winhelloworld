﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Windows.Forms;


namespace HelloWorld
{
    public partial class Form1 : Form
    {
        private class VisionARDLL
        {
            [DllImport("VisionARFB.dll", CharSet = CharSet.Unicode, ExactSpelling = true)]
            public static extern bool Startup();
            [DllImport("VisionARCDC.dll", CharSet = CharSet.Unicode, ExactSpelling = true)]
            public static extern bool StartupCdc();
            [DllImport("VisionARFB.dll", EntryPoint = "CloseFb", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool CloseDevFb();
            [DllImport("VisionARCDC.dll", EntryPoint = "CloseCdc", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool CloseDevCdc();
            [DllImport("VisionARFB.dll", EntryPoint = "WriteImg", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool WriteImg(byte[] data);
            [DllImport("VisionARCDC.dll", EntryPoint = "Haptic", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool Haptic(int time);
        }

        public Form1()
        {
            InitializeComponent();
            if (!VisionARDLL.Startup() || !VisionARDLL.StartupCdc())
            {
                MessageBox.Show("Please make sure the glasses are connected", "Glasses not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(-1);
            }
        }


        private readonly Bitmap bmp1 = new Bitmap(Properties.Resources.batman);
        private readonly Bitmap bmp2 = new Bitmap(Properties.Resources.hello_world);
        private readonly Bitmap bmp3 = new Bitmap(Properties.Resources.illusion);
        private readonly Bitmap bmp4 = new Bitmap(Properties.Resources.map);
        private readonly Bitmap bmp5 = new Bitmap(Properties.Resources.mix);
        private readonly Bitmap bmp6 = new Bitmap(Properties.Resources.mountain);
        private readonly Bitmap bmp7 = new Bitmap(Properties.Resources.tag);
        private readonly Bitmap bmp8 = new Bitmap(Properties.Resources.visionar);

        private int bmpNum = 1;


        private static byte[] BitmapToByteArray(Bitmap img)
        {
            Rectangle rect = new Rectangle(0, 0, img.Width, img.Height);
            BitmapData bitmapData = img.LockBits(rect, ImageLockMode.ReadOnly, img.PixelFormat);
            int length = bitmapData.Stride * bitmapData.Height;

            byte[] bytes = new byte[length];

            Marshal.Copy(bitmapData.Scan0, bytes, 0, length);
            img.UnlockBits(bitmapData);
            return bytes;
        }


        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!VisionARDLL.CloseDevFb())  Console.WriteLine("error devicefb close");
            if (!VisionARDLL.CloseDevCdc()) Console.WriteLine("error devicecdc close");
        }

        /*
         * param: inputBmp -> Bitmap object to display on pictureBox1
         * return: 419x138 resized bitmap
         */
        private Bitmap SendToPictureBox(Bitmap inputBmp)
        {
            Bitmap outputBmp = new Bitmap(inputBmp, pictureBox1.Width, pictureBox1.Height);

            pictureBox1.Image = outputBmp;
            pictureBox1.Refresh();
            return outputBmp;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (bmpNum > 8) bmpNum = 1;

            switch (bmpNum)
            {
                case 1:
                    byte[] bytes1 = BitmapToByteArray(SendToPictureBox(bmp1));

                    if (!VisionARDLL.WriteImg(bytes1))
                    {
                        Console.WriteLine("error WriteImg");
                    }

                    break;

                case 2:
                    byte[] bytes2 = BitmapToByteArray(SendToPictureBox(bmp2));

                    if (!VisionARDLL.WriteImg(bytes2))
                    {
                        Console.WriteLine("error WriteImg");
                    }

                    break;

                case 3:
                    byte[] bytes3 = BitmapToByteArray(SendToPictureBox(bmp3));

                    if (!VisionARDLL.WriteImg(bytes3))
                    {
                        Console.WriteLine("error WriteImg");
                    }

                    break;

                case 4:
                    byte[] bytes4 = BitmapToByteArray(SendToPictureBox(bmp4));

                    if (!VisionARDLL.WriteImg(bytes4))
                    {
                        Console.WriteLine("error WriteImg");
                    }

                    break;

                case 5:
                    byte[] bytes5 = BitmapToByteArray(SendToPictureBox(bmp5));

                    if (!VisionARDLL.WriteImg(bytes5))
                    {
                        Console.WriteLine("error WriteImg");
                    }

                    break;

                case 6:
                    byte[] bytes6 = BitmapToByteArray(SendToPictureBox(bmp6));

                    if (!VisionARDLL.WriteImg(bytes6))
                    {
                        Console.WriteLine("error WriteImg");
                    }

                    break;

                case 7:
                    byte[] bytes7 = BitmapToByteArray(SendToPictureBox(bmp7));

                    if (!VisionARDLL.WriteImg(bytes7))
                    {
                        Console.WriteLine("error WriteImg");
                    }

                    break;

                case 8:
                    byte[] bytes8 = BitmapToByteArray(SendToPictureBox(bmp8));

                    if (!VisionARDLL.WriteImg(bytes8))
                    {
                        Console.WriteLine("error WriteImg");
                    }

                    break;
            }

            if (!VisionARDLL.Haptic(100)) Console.WriteLine("error Haptic");

            bmpNum++;
        }
    }
}
