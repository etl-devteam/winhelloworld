; CheckAndDownloadVCRedist.nsh
;
; It will install "Hello, world" application for Windows into a directory that the user selects,
; creating start menu and desktop links, according to user choice.

!pragma warning disable 6010

;--------------------------------
Function CheckAndDownloadVCRedist
# Let's see if the user has the VC Redist libraries installed, according to $0 version on their
# system or not.
# Remember: you need Vista SP2 or 7 SP1.  It is built in to Windows 8, and not needed
# In case you're wondering, running this code on Windows 8 will correctly return is_equal
# or is_greater (maybe Microsoft releases .NET 4.5 SP1 for example)

pop $0 ; VC Redist number
pop $1 ; installer URL

# Set up our Variables
Var /GLOBAL vcRedistIsThere
Var /GLOBAL vcRedist_CMD_LINE
Var /GLOBAL vcRedist_EXIT_CODE

ReadRegDWORD $vcRedistIsThere HKLM "SOFTWARE\WOW6432Node\Microsoft\VisualStudio\14.0\VC\Runtimes\X64" "Major"

IntCmp $vcRedistIsThere $0 is_equal is_less is_greater

is_equal:
    Goto done_compare_not_needed
is_greater:
    # Useful if, for example, Microsoft releases .NET 4.5 SP1
    # We want to be able to simply skip install since it's not
    # needed on this system
    Goto done_compare_not_needed
is_less:
    Goto done_compare_needed

done_compare_needed:
    #VC Redist install is *NEEDED*

    # Setup looks for installer
    # This allows the installer to be placed on a USB stick (for computers without internet connections)
    # If the .NET Framework 4.5 installer is *NOT* found, Setup will connect to Microsoft's website
    # and download it for you

    # Reboot Required with these Exit Codes:
    # 1641 or 3010

    # Command Line Switches:
    # /showrmui /passive /norestart

    # Silent Command Line Switches:
    # /q /norestart


    # Let's see if the user is doing a Silent install or not
    IfSilent is_quiet is_not_quiet

    is_quiet:
        StrCpy $vcRedist_CMD_LINE "/q /norestart"
        Goto do_network_install
    is_not_quiet:
        StrCpy $vcRedist_CMD_LINE "/showrmui /passive /norestart"
        Goto do_network_install

    # Now, let's Download the VC Redist
    do_network_install:
        inetc::get $1 "$TEMP\vc_redist.x64.exe"
	    Pop $R0
        StrCmp $R0 "OK" success
            MessageBox MB_OK|MB_ICONEXCLAMATION "Unable to download VC Redist libraries. Product will be installed, but will not function without the Framework!"
            Goto done_VCRedist_function

	success:
        ExecWait '"$TEMP\vc_redist.x64.exe" $vcRedist_CMD_LINE' $vcRedist_EXIT_CODE
        # $vcRedist_EXIT_CODE contains the return codes.  1641 and 3010 means a Reboot has been requested
        IntCmp $vcRedist_EXIT_CODE 1641 reboot_required
        IntCmp $vcRedist_EXIT_CODE 3010 reboot_required
        Goto done_VCRedist_function

    reboot_required:
        SetRebootFlag true

done_compare_not_needed:
    # Done VC Redist Install
    Goto done_VCRedist_function

#exit the function
done_VCRedist_function:

FunctionEnd


Function CheckAndDownloadVCRedist2015

Push https://aka.ms/vs/17/release/vc_redist.x64.exe
Push 14
Call CheckAndDownloadVCRedist

FunctionEnd


Function CheckAndDownloadVCRedist2013

Push https://aka.ms/highdpimfc2013x64enu
Push 12
Call CheckAndDownloadVCRedist

FunctionEnd

Function CheckAndDownloadVCRedist2012

Push https://download.microsoft.com/download/1/6/B/16B06F60-3B20-4FF2-B699-5E9B7962F9AE/VSU_4/vcredist_x64.exe
Push 11
Call CheckAndDownloadVCRedist

FunctionEnd
